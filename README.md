## Terraform Course

Recipe App API Proxy

NGINX

## Usage


#### GIT

cd recipe-app-api-proxy\
git add .\
git commit -m "data"\
git push origin\
git checkout -b feature/nginx-proxy


#### DOCKER 
docker build -t proxy .

### ENV Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)


## entrpoint.sh

Populates nginx default.conf.tpl with env variables. Starts with `#!/bin/sh` so Docker now that it has to execute it.

# Dockerfile 

`RUN chmod +x /entrypoint.sh` - makes the file executable


